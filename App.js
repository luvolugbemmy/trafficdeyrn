import React from 'react';
import { AppRegistry, StyleSheet, Text, View, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import SideMenu from 'react-native-side-menu';
import { Icon } from 'react-native-elements'
// import FontAwesome, { Icons } from 'react-native-fontawesome';
import Menu from './Menu';
import { styles } from './App.Styles';
import Login from './src/components/login/Login';
import TrafficReportList from './src/components/traffic-reports/TrafficReportList';


// const image = require('./assets/menu.png');
const auth_token = null;
AsyncStorage.getItem('_traffic_dey_authentication_token').then((token) => {
  auth_token = token;
});
export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false,
      auth_token: auth_token,
      isLoggedIn: (auth_token ? true : false),
      selectedItem: 'About',
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  updateMenuState(isOpen) {
    this.setState({ isOpen });
  }

  onMenuItemSelected = item =>
    this.setState({
      isOpen: false,
      selectedItem: item,
    });
  
  
  render() {    

    return (
      <RootPage isLoggedIn={this.state.isLoggedIn} ctrl={this} />
    );
  }

}

function LoggedIn(props){
  const menu = <Menu onItemSelected={props.ctrl.onMenuItemSelected} />;
  return (
      <SideMenu
        menu={menu}
        isOpen={props.ctrl.state.isOpen}
        onChange={isOpen => props.ctrl.updateMenuState(isOpen)}
      >
        <TrafficReportList />
        <TouchableOpacity
          onPress={props.ctrl.toggle}
          style={styles.button}          
        >
          <Icon name='menu' style={{width: 32, height: 32}}/>            
        </TouchableOpacity>
      </SideMenu>
  )
}

function RootPage(props) {
  const isLoggedIn = props.isLoggedIn;
  if (isLoggedIn) {
    return <LoggedIn ctrl={props.ctrl} />; //<PropertyList />;
  }
  return <Login />;
}

AppRegistry.registerComponent('App', () => App);
          // <Image
          //   source={image}
          //   style={{ width: 32, height: 32 }}
          // />
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   }, <Greeting isLoggedIn={isLoggedIn} />
// });

// import Icon from 'react-native-vector-icons/FontAwesome';
// const myButton = (
//      <Icon.Button name="facebook" backgroundColor="#3b5998" onPress={this.loginWithFacebook}>
//       Login with Facebook
//       </Icon.Button>
//    );