import React from 'react';
import { AppRegistry, AsyncStorage, Animated, Easing, StyleSheet, Text, View, Image, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import SideMenu from 'react-native-side-menu';
import { Icon, Button, SocialIcon } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';
import TrafficReport from './TrafficReport';
import { HEADERS } from './../fetchHeaders';
import PropTypes from 'prop-types'

const segments = ['Route', 'Pinned Locations'];
const config = require('./../../../config.json');
export default class TrafficReportList extends React.Component {
  constructor(props) {
    super(props);
    this.getCoords = this.getCoords.bind(this);
    this.state = {
      selected: "Route",
      report_limit_start: 0,
      report_limit_size: 20,
      report_count: -1,
      distance: 1300,
      latitude: null,
      longitude: null,
      error: null
    };
  }

  fetchTrafficReports(){
    let params = [];
    params.push("limit_start="+this.state.report_limit_start);
    params.push("limit_size="+this.state.report_limit_size);
    params.push("latitude="+this.state.latitude);
    params.push("longitude="+this.state.longitude);
    params.push("distance="+this.state.distance); 
    console.log(HEADERS);   
    fetch(config.API_URL+"traffic_reports?"+params.join("&"), {  
      method: 'GET',
      headers: HEADERS
    }).then((response) => response.json()).then((resp) => {
      console.log(resp)
      if (resp && resp.code == 200) {
        try {         
          console.log(resp);
        } catch (error) {
          alert(JSON.stringify(error))
        }
      }else {
        alert(JSON.stringify(resp))
      }     
    }).catch((error) => {
      console.error(error);
    });
  }

  componentDidMount() {
    this.fetchTrafficReports();        
  }

  getCoords(){
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });       
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );    
  }
  
  
  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>

      </KeyboardAvoidingView>
    );
  }
}

// TrafficReportList.propTypes = {
//  propArray: PropTypes.array.isRequired, 
//  propBool: PropTypes.bool.isRequired,
//  propFunc: PropTypes.func,
//  propNumber: PropTypes.number,
//  propString: PropTypes.string,
//  propObject: PropTypes.object
// }
        

const styles = StyleSheet.create({
  container: {    

  }
});

AppRegistry.registerComponent('TrafficReportList', () => TrafficReportList);