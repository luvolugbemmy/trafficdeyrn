import { AsyncStorage } from 'react-native';
let config = require('./../../config.json');
export const HEADERS = config.API_HEADERS;
AsyncStorage.getItem('_traffic_dey_authentication_token').then((key) => {
  HEADERS['X-USER-TOKEN'] = ""+key;
})
AsyncStorage.getItem('_traffic_dey_user_session')
.then((sess) => JSON.parse(sess))
.then((session) => {
  HEADERS['X-USER-EMAIL'] = session.email;
})