import { StyleSheet } from 'react-native';
export const styles = StyleSheet.create({
  button: {
    position: 'absolute',
    top: 20,
    padding: 10,
  },
  caption: {
    fontSize: 20,
    fontWeight: 'bold',
    alignItems: 'center',
  },
  bgContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {    

  },
  signUPB: {
    margin: 20,
    padding: 15,
    borderWidth: 1,    
    borderColor: '#333',
  },
  socialButtons:{
    width: 48, 
    height: 48,
  },
  buttonText:{
      textAlign: 'center',
      fontWeight: '700'
  },
  splashBg: {
    position: 'relative',    
    height: 180,
    zIndex: 1,
    backgroundColor: "transparent",
    // resizeMode: 'cover',
  },
  splashInfo: {
    position: 'relative',
    zIndex: 2,
    marginTop: -32,
    alignItems: 'center',
  },
  splashLogo: {
    margin: 'auto',
    width: 64,
    height: 64,
  },
  splashIntro: {
    fontSize: 11,
    fontStyle: 'italic',
    fontWeight: 'bold',
    maxWidth: '80%',
    margin: 'auto',
    color: '#222',
  }
});