import React from 'react'
import { AppRegistry, AsyncStorage, View, Text, TextInput, TouchableOpacity, StyleSheet} from 'react-native';
import base64 from 'base-64';
import { styles } from './LoginForm.Styles';

const config = require('./../../../config.json');

export default class LoginForm extends React.Component{
  constructor(props) {
    super(props);
    this.onButtonPress = this.onButtonPress.bind(this);
    this.state = {
      isOpen: false,
      selectedItem: 'About',
    };
  }

	onButtonPress(){
		fetch(config.API_URL+'users/login', {  
		  method: 'POST',
		  headers: config.API_HEADERS,
		  body: JSON.stringify({
		    login: 'traffic_angel',
		    password: ('Password1' ? base64.encode('Password1') : null)
		  })
		}).then((response) => response.json()).then((resp) => {
			if (resp && resp.code == 200) {
				try {					
				  AsyncStorage.setItem('_traffic_dey_authentication_token', resp.data.authentication_token, (cb) => {console.log(cb)});
				  AsyncStorage.setItem('_traffic_dey_user_session', JSON.stringify(resp.data.current_user));
				} catch (error) {
				  alert(JSON.stringify(error))
				}
			}else {
				alert(JSON.stringify(resp))
			}			
		}).catch((error) => {
      console.error(error);
    });
	}
// keyboardType='email-address' 
	render() {
		return (
			<View style={styles.container}>
				<TextInput style = {styles.input} 
				               autoCapitalize="none" 
				               onSubmitEditing={() => this.passwordInput.focus()} 
				               autoCorrect={false} 				               
				               returnKeyType="next" 
				               placeholder='Username' 
				               placeholderTextColor='rgba(225,225,225,0.7)'/>

				<TextInput style = {styles.input}   
				              returnKeyType="go" 
				              ref={(input)=> this.passwordInput = input} 
				              placeholder='Password' 
				              placeholderTextColor='rgba(225,225,225,0.7)' 
				              secureTextEntry/>

				<TouchableOpacity style={styles.buttonContainer} 
				                     onPress={this.onButtonPress}>
				             <Text  style={styles.buttonText}>LOGIN</Text>
				</TouchableOpacity> 			
			</View>	
		);
	}
}

AppRegistry.registerComponent('LoginForm', () => LoginForm);