import React from 'react';
import { AppRegistry, Animated, Easing, StyleSheet, Text, View, Image, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import SideMenu from 'react-native-side-menu';
import { Icon, Button, SocialIcon } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';
import LoginForm from './LoginForm';
import { styles } from './Login.Styles';


export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.forgotPassword = this.forgotPassword.bind(this);
    this.state = {
      isOpen: false,
      selectedItem: 'About'
    };
  }

  forgotPassword(){

  }

  componentDidMount() {

  }
  
  
  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <View style={styles.bgContainer}>
          <Animatable.Image
            animation="slideOutLeft"
            direction="alternate"          
            iterationCount="infinite"
            easing="ease-in-out-sine"          
            source={splashBGImage}
            style={styles.splashBg}
          >
          </Animatable.Image>
        </View>
        <View style={styles.splashInfo}>
          <Image source={splashLogoImage} style={styles.splashLogo}></Image>
          <View>
            <Text style={styles.splashIntro}>Drive better with Traffic-Dey</Text>
          </View>
        </View>               
        <LoginForm />
        <View style={{alignItems: 'center'}}>        
          <TouchableOpacity style={{padding: 10}}
            onPress={this.forgotPassword}>
              <Text style={{fontWeight: 'bold'}}>Forgot password ?</Text>      
          </TouchableOpacity>
          <TouchableOpacity style={styles.signUPB}>
                       <Text  style={styles.buttonText}>SIGN UP</Text>
          </TouchableOpacity>
          <View style={{padding: 15,alignItems: 'center'}}>
            <Text style={{marginBottom: 10}}>Sign in with:</Text>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <TouchableOpacity>
                  <SocialIcon button type='facebook' style={styles.socialButtons}/>             
              </TouchableOpacity>
              <TouchableOpacity>
                <SocialIcon button type='google-plus-official' style={styles.socialButtons}/>                          
              </TouchableOpacity>
              <TouchableOpacity>
                <SocialIcon button type='twitter' style={styles.socialButtons}/>
              </TouchableOpacity>
            </View>
          </View>         
        </View>
      </KeyboardAvoidingView>
    );
  }
}

        
const splashBGImage = require('./../../assets/images/splashbg.png');
const splashLogoImage = require('./../../assets/images/appicon.png');

AppRegistry.registerComponent('Login', () => Login);